package treedisplay

import "gitlab.com/atrico/trees/v2"

type showPathPosition string

const (
	iAmShowPath          showPathPosition = "I am the showPath" // This node is showPath
	childShowPathAbove   showPathPosition = "Child above"       // child above is showPath
	childShowPathBelow   showPathPosition = "Child below"       // child below is showPath
	siblingShowPathAbove showPathPosition = "Sibling above"     // sibling above is showPath
	siblingShowPathBelow showPathPosition = "Sibling below"     // sibling below is showPath
)

func (hl showPathPosition) String() string {
	return string(hl)
}

type showPathPositionSet map[showPathPosition]any

func processShowPathRoot[T any](node tree.Node[T], showPaths []tree.PathToNode[T], config DisplayTreeConfig[T]) (showPathState showPathPositionSet, showPathChildIndices []int, showPathsTail []tree.PathToNode[T]) {
	showPathState = make(showPathPositionSet)
	for _, showPath := range showPaths {
		if len(showPath) > 0 && node.Equals(showPath[0]) {
			showPathsTail = append(showPathsTail, showPath[1:])
			if len(showPath) == 1 {
				showPathState[iAmShowPath] = nil
			} else {
				_, showPathChildIdx := tree.Contains(node.Children(), showPath[1])
				showPathChildIndices = append(showPathChildIndices, showPathChildIdx)
				childrenAbove := numberOfChildren(node, config).above
				if showPathChildIdx < childrenAbove {
					showPathState[childShowPathAbove] = nil
				} else {
					showPathState[childShowPathBelow] = nil
				}
			}
		}
	}

	return showPathState, showPathChildIndices, showPathsTail
}

func processShowPathNode[T any](node tree.Node[T], parent nodeDetails[T], idx int, config DisplayTreeConfig[T]) (showPathState showPathPositionSet, showPathChildIndices []int, showPathsTail []tree.PathToNode[T]) {
	showPathState, showPathChildIndices, showPathsTail = processShowPathRoot(node, parent.remainingShowPaths, config)
	if parent.hasShowPathChild() {
		// Are both this child and showPath child above or below
		if (idx < parent.children.above) == parent.hasShowPathChildAbove() {
			for _, childIdx := range parent.showPathChildIndices {
				if idx < childIdx {
					showPathState[siblingShowPathBelow] = nil
				} else if idx > childIdx {
					showPathState[siblingShowPathAbove] = nil
				}
			}
		}
	}
	return showPathState, showPathChildIndices, showPathsTail
}
