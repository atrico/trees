package treedisplay

import (
	"fmt"
	"strings"

	"gitlab.com/atrico/console/box_drawing"

	"gitlab.com/atrico/trees/v2"
)

func DisplayTree[T any](root tree.Node[T], config DisplayTreeConfig[T]) []string {
	var showPaths []tree.PathToNode[T]
	if config.ShowPath != nil {
		showPaths = tree.FindAll[T](root, config.ShowPath)
	}
	return displayTree(newRootDetails[T](root, showPaths, config), config)
}

func DisplayBinaryTree[T any](root tree.BinaryNode[T], config DisplayTreeConfig[T]) []string {
	return DisplayTree(tree.BinaryNodeWrapper[T]{BinaryNode: root}, config)
}

func displayTree[T any](details nodeDetails[T], config DisplayTreeConfig[T]) []string {
	lines := make([]string, 0, 1)
	for i := 0; i < details.children.above; i++ {
		lines = append(lines, displayTree(newNodeDetails(&details, i, config), config)...)
	}
	lines = append(lines, formatNode(details, config))
	for i := details.children.above; i < details.children.total; i++ {
		lines = append(lines, displayTree(newNodeDetails(&details, i, config), config)...)
	}
	return lines
}

func formatNode[T any](details nodeDetails[T], config DisplayTreeConfig[T]) string {
	text := strings.Builder{}
	// Prefix (previous branches)
	text.WriteString(formatPrefix(details.parent, details.parentPosition))
	// Attachment to tree structure
	if !details.isRoot() {
		if details.parentPosition == aboveParent && details.siblingLocation == firstSibling {
			text.WriteRune(box_drawing.MustGetBoxChar(false, true, false, true, box_drawing.BoxSingle.HeavyIf(details.isOnShowPath()))) // ┌
		} else if details.parentPosition == belowParent && details.siblingLocation == lastSibling {
			text.WriteRune(box_drawing.MustGetBoxChar(true, false, false, true, box_drawing.BoxSingle.HeavyIf(details.isOnShowPath()))) // └
		} else {
			_, showPathSiblingAbove := details.showPathState[siblingShowPathAbove]
			_, showPathSiblingBelow := details.showPathState[siblingShowPathBelow]
			onShowPath := details.isOnShowPath() || details.isShowPathed()
			passThrough := (showPathSiblingAbove && details.parentPosition == aboveParent) || (showPathSiblingBelow && details.parentPosition == belowParent)
			fromTop := onShowPath && details.parentPosition == belowParent
			fromBottom := onShowPath && details.parentPosition == aboveParent
			text.WriteRune(box_drawing.MustGetBoxCharMixed(box_drawing.BoxParts{
				Up:    box_drawing.BoxSingle.HeavyIf(passThrough || fromTop),
				Down:  box_drawing.BoxSingle.HeavyIf(passThrough || fromBottom),
				Left:  box_drawing.BoxNone,
				Right: box_drawing.BoxSingle.HeavyIf(onShowPath)})) // ├
		}
	}
	// Dash
	text.WriteRune(box_drawing.MustGetBoxChar(false, false, true, true, box_drawing.BoxSingle.HeavyIf(details.isOnShowPath()))) // -
	// Name of node (and branch chars)
	parts := box_drawing.BoxParts{}
	parts.Up = box_drawing.ConditionalBoxType(details.children.above > 0, box_drawing.BoxSingle.HeavyIf(details.hasShowPathChildAbove()), box_drawing.BoxNone)
	parts.Down = box_drawing.ConditionalBoxType(details.children.below > 0, box_drawing.BoxSingle.HeavyIf(details.hasShowPathChildBelow()), box_drawing.BoxNone)
	parts.Left = box_drawing.BoxSingle.HeavyIf(details.isOnShowPath())
	value, valuePresent := details.node.GetNodeValue()
	parts.Right = box_drawing.ConditionalBoxType(valuePresent || len(details.node.Children()) == 0, box_drawing.BoxSingle.HeavyIf(details.isShowPathed()), box_drawing.BoxNone)
	text.WriteRune(box_drawing.MustGetBoxCharMixed(parts))
	if valuePresent {
		text.WriteRune(box_drawing.MustGetBoxChar(false, false, false, false, box_drawing.BoxNone))
		if config.Formatter != nil {
			text.WriteString(config.Formatter(details.node))
		} else {
			text.WriteString(fmt.Sprintf("%v", value))
		}
	}

	return text.String()
}

func formatPrefix[T any](details *nodeDetails[T], context parentPosition) string {
	prefix := strings.Builder{}
	if details != nil {
		// Root level has no prefix
		if !details.isRoot() {
			prefix.WriteString(formatPrefix(details.parent, details.parentPosition))
			bypass := false
			switch details.siblingLocation {
			// First has bypass if calling child is below
			case firstSibling:
				bypass = context == belowParent
			// Mid sibling must have bypass
			case midSibling:
				bypass = true
			// Last has bypass if calling child is above
			case lastSibling:
				bypass = context == aboveParent
			}
			if bypass {
				showPath := (context == belowParent && details.isOnShowPath() && details.parentPosition == aboveParent) ||
					(context == aboveParent && details.isOnShowPath() && details.parentPosition == belowParent) ||
					(context == belowParent && details.hasShowPathSiblingBelow()) ||
					(context == aboveParent && details.hasShowPathSiblingAbove())
				prefix.WriteRune(box_drawing.MustGetBoxChar(true, true, false, false, box_drawing.BoxSingle.HeavyIf(showPath))) // |
			} else {
				prefix.WriteRune(box_drawing.MustGetBoxChar(false, false, false, false, box_drawing.BoxNone)) // space
			}
		}
		prefix.WriteRune(box_drawing.MustGetBoxChar(false, false, false, false, box_drawing.BoxNone)) // space
	}
	return prefix.String()
}
