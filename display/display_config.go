package treedisplay

import (
	"gitlab.com/atrico/trees/v2"
)

type DisplayTreeConfig[T any] struct {
	Type       DisplayType
	Characters CharacterType
	ShowPath   tree.NodeMatcher[T]
	// Format the value of a node (add color, etc)
	Formatter NodeFormatter[T]
}

func NewDisplayConfig[T any]() DisplayTreeConfig[T] {
	return DisplayTreeConfig[T]{Type: Balanced, Characters: Unicode, ShowPath: nil}
}

func (config DisplayTreeConfig[T]) WithDisplayType(value DisplayType) DisplayTreeConfig[T] {
	return DisplayTreeConfig[T]{Type: value, Characters: config.Characters, ShowPath: config.ShowPath, Formatter: config.Formatter}
}

func (config DisplayTreeConfig[T]) WithCharacterType(value CharacterType) DisplayTreeConfig[T] {
	return DisplayTreeConfig[T]{Type: config.Type, Characters: value, ShowPath: config.ShowPath, Formatter: config.Formatter}
}

func (config DisplayTreeConfig[T]) WithShowPath(matcher tree.NodeMatcher[T]) DisplayTreeConfig[T] {
	return DisplayTreeConfig[T]{Type: config.Type, Characters: config.Characters, ShowPath: matcher, Formatter: config.Formatter}
}

func (config DisplayTreeConfig[T]) WithFormatter(formatter func(node tree.Node[T]) string) DisplayTreeConfig[T] {
	return DisplayTreeConfig[T]{Type: config.Type, Characters: config.Characters, ShowPath: config.ShowPath, Formatter: formatter}
}

type DisplayType int
type CharacterType int

const (
	TopDown           DisplayType   = iota
	Balanced          DisplayType   = iota
	BalancedFavourTop DisplayType   = iota
	BottomUp          DisplayType   = iota
	Unicode           CharacterType = iota
)
