package treedisplay

import (
	"fmt"

	"gitlab.com/atrico/console/ansi"
	tree "gitlab.com/atrico/trees/v2"
)

type NodeFormatter[T any] func(node tree.Node[T]) string
type ValueFormatter[T any] func(value T) string

type NodeFormatterBuilder[T any] interface {
	Add(matcher tree.NodeMatcher[T], valueFormatter ValueFormatter[T]) NodeFormatterBuilder[T]
	AddAttributes(matcher tree.NodeMatcher[T], attributes ansi.Attributes) NodeFormatterBuilder[T]
	Build() NodeFormatter[T]
}

func NewNodeFormatterBuilder[T any]() NodeFormatterBuilder[T] {
	return &formatter[T]{}
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------

type formatter[T any] struct {
	matchers   []tree.NodeMatcher[T]
	formatters []ValueFormatter[T]
}

func (f *formatter[T]) Add(matcher tree.NodeMatcher[T], valueFormatter ValueFormatter[T]) NodeFormatterBuilder[T] {
	f.matchers = append(f.matchers, matcher)
	f.formatters = append(f.formatters, valueFormatter)
	return f
}

func (f *formatter[T]) AddAttributes(matcher tree.NodeMatcher[T], attributes ansi.Attributes) NodeFormatterBuilder[T] {
	return f.Add(matcher, func(value T) string {
		return attributes.ApplyWithResetTo(fmt.Sprintf("%v", value))
	})
}

func (f *formatter[T]) Build() NodeFormatter[T] {
	return func(node tree.Node[T]) string {
		if value, present := node.GetNodeValue(); present {
			for i, matcher := range f.matchers {
				if matcher.IsMatch(node) {
					return f.formatters[i](value)
				}
			}
			return fmt.Sprintf("%v", value)
		}
		return ""
	}
}
