package treedisplay

import "gitlab.com/atrico/trees/v2"

type parentPosition string
type siblingLocation string

const (
	aboveParent  parentPosition  = "Above parent" // Node is above the parent
	belowParent  parentPosition  = "Below parent" // Node is below the parent
	noParent     parentPosition  = "No Parent"
	firstSibling siblingLocation = "First sibling"
	midSibling   siblingLocation = "Mid sibling"
	lastSibling  siblingLocation = "Last sibling"
)

func (pp parentPosition) String() string {
	return string(pp)
}
func (sl siblingLocation) String() string {
	return string(sl)
}

type numberSplit struct {
	above int
	below int
	total int
}

type nodeDetails[T any] struct {
	parent               *nodeDetails[T]
	node                 tree.Node[T]
	parentPosition       parentPosition
	siblingLocation      siblingLocation
	children             numberSplit
	showPathState        showPathPositionSet
	showPathChildIndices []int
	remainingShowPaths   []tree.PathToNode[T]
}

func newRootDetails[T any](node tree.Node[T], showPaths []tree.PathToNode[T], config DisplayTreeConfig[T]) nodeDetails[T] {
	showPathPositionSet, showPathChildIndices, showPathsTail := processShowPathRoot(node, showPaths, config)
	return nodeDetails[T]{nil, node, noParent, midSibling, numberOfChildren(node, config), showPathPositionSet, showPathChildIndices, showPathsTail}
}

func newNodeDetails[T any](parent *nodeDetails[T], idx int, config DisplayTreeConfig[T]) nodeDetails[T] {
	node := parent.node.Children()[idx]
	showPathPositionSet, showPathChildIdx, showPathsTail := processShowPathNode(node, *parent, idx, config)

	siblings := numberOfChildren(parent.node, config)
	var parentPosition parentPosition
	siblingLocation := midSibling
	if idx < siblings.above {
		parentPosition = aboveParent
		if idx == 0 {
			siblingLocation = firstSibling
		}
	} else {
		parentPosition = belowParent
		if idx == len(parent.node.Children())-1 {
			siblingLocation = lastSibling
		}
	}
	return nodeDetails[T]{parent, node, parentPosition, siblingLocation, numberOfChildren(node, config), showPathPositionSet, showPathChildIdx, showPathsTail}
}

func (d nodeDetails[T]) isRoot() bool {
	return d.parentPosition == noParent
}

func (d nodeDetails[T]) isShowPathed() (result bool) {
	_, result = d.showPathState[iAmShowPath]
	return result
}

func (d nodeDetails[T]) hasShowPathChildAbove() (result bool) {
	_, result = d.showPathState[childShowPathAbove]
	return result
}
func (d nodeDetails[T]) hasShowPathChildBelow() (result bool) {
	_, result = d.showPathState[childShowPathBelow]
	return result
}
func (d nodeDetails[T]) hasShowPathChild() bool {
	return d.hasShowPathChildAbove() || d.hasShowPathChildBelow()
}
func (d nodeDetails[T]) isOnShowPath() bool {
	return d.isShowPathed() || d.hasShowPathChild()
}
func (d nodeDetails[T]) hasShowPathSiblingAbove() (result bool) {
	_, result = d.showPathState[siblingShowPathAbove]
	return result
}
func (d nodeDetails[T]) hasShowPathSiblingBelow() (result bool) {
	_, result = d.showPathState[siblingShowPathBelow]
	return result
}

// ---------------------------------------------------------------
// Internal functions
// ---------------------------------------------------------------
func numberOfChildren[T any](node tree.Node[T], config DisplayTreeConfig[T]) numberSplit {
	total := len(node.Children())
	var above int
	switch config.Type {
	case TopDown:
		above = 0
	case Balanced:
		above = total / 2
	case BalancedFavourTop:
		above = (total + 1) / 2
	case BottomUp:
		above = total
	default:
		panic("Unrecognised display type")
	}
	return numberSplit{above, total - above, total}
}
