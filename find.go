package tree

type PathToNode[T any] []Node[T]

// Find the first instance of a node
// Return true if found and path to the node
func FindFirst[T any](node Node[T], matcher NodeMatcher[T]) (path PathToNode[T], found bool) {
	if matcher.IsMatch(node) {
		return []Node[T]{node}, true
	}
	for _, child := range node.Children() {
		nodes, result := FindFirst(child, matcher)
		if result {
			return append([]Node[T]{node}, nodes...), true
		}
	}
	return nil, false
}

func MustFindFirst[T any](node Node[T], matcher NodeMatcher[T]) (path PathToNode[T]) {
	path, found := FindFirst(node, matcher)
	if !found {
		panic("not found")
	}
	return path
}

// Find all matching nodes
func FindAll[T any](node Node[T], matcher NodeMatcher[T]) (paths []PathToNode[T]) {
	if matcher.IsMatch(node) {
		paths = []PathToNode[T]{{node}}
	}
	for _, child := range node.Children() {
		for _, subPath := range FindAll(child, matcher) {
			paths = append(paths, append([]Node[T]{node}, subPath...))
		}
	}

	return paths
}
