package tree

type NodeMatcher[T any] interface {
	IsMatch(node Node[T]) bool
}

func MakeNodeMatcher[T any](f func(node Node[T]) bool) NodeMatcher[T] {
	return nodeMatcherFunc[T](f)
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------
type nodeMatcherFunc[T any] func(node Node[T]) bool

func (n nodeMatcherFunc[T]) IsMatch(node Node[T]) bool {
	return n(node)
}
