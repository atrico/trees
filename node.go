package tree

import "fmt"

// Basic tree node
type Node[T any] interface {
	// The value associated with this node
	GetNodeValue() (value T, ok bool)
	MustGetNodeValue() (value T)
	// This node's children
	Children() []Node[T]
	// Is this node the same node (or copy of)
	Equals(rhs Node[T]) bool
}

var NodeHasNoValueError error = fmt.Errorf("node does not contain a value")

func Contains[T any](list []Node[T], node Node[T]) (found bool, idx int) {
	for i, n := range list {
		if n.Equals(node) {
			return true, i
		}
	}
	return false, -1
}
