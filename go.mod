module gitlab.com/atrico/trees/v2

go 1.24

require (
	github.com/google/uuid v1.6.0
	gitlab.com/atrico/console v1.12.3
	gitlab.com/atrico/testing/v2 v2.5.3
)

require golang.org/x/exp v0.0.0-20250218142911-aa4b98e5adaa // indirect
