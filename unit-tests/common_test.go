package unit_tests

import (
	"fmt"

	"gitlab.com/atrico/testing/v2/random"

	"github.com/google/uuid"
	"gitlab.com/atrico/trees/v2"
)

var rg = random.NewValueGenerator()

// Create matcher that checks for equal string value
func nodeMatcher(value string) tree.NodeMatcher[string] {
	return tree.MakeNodeMatcher[string](func(node tree.Node[string]) bool {
		val, present := node.GetNodeValue()
		return present && val == value
	})
}

func NodeValueAsString[T any](node tree.Node[T]) (str string) {
	if value, present := node.GetNodeValue(); present {
		str = fmt.Sprintf("%v", value)
	}
	return
}

// -------------------------------------------------------------------------------------------------
// Tree
// -------------------------------------------------------------------------------------------------

type testTreeNode struct {
	uid      uuid.UUID
	value    string
	children []tree.Node[string]
}

func (n testTreeNode) String() (str string) {
	if value, present := n.GetNodeValue(); present {
		str = fmt.Sprintf("%v", value)
	}
	return
}

func (n testTreeNode) GetNodeValue() (value string, ok bool) {
	return n.value, n.value != ""
}
func (n testTreeNode) MustGetNodeValue() (value string) {
	var ok bool
	if value, ok = n.GetNodeValue(); !ok {
		panic(tree.NodeHasNoValueError)
	}
	return
}

func (n testTreeNode) Children() []tree.Node[string] {
	return n.children
}

func createNode(value string, children ...tree.Node[string]) tree.Node[string] {
	return testTreeNode{uuid.New(), value, children}
}

func createEmptyNode(children ...tree.Node[string]) tree.Node[string] {
	return testTreeNode{uuid.New(), "", children}
}

func (n testTreeNode) Equals(rhs tree.Node[string]) bool {
	nt, ok := rhs.(testTreeNode)
	return ok && n.uid == nt.uid
}

// -------------------------------------------------------------------------------------------------
// Binary tree
// -------------------------------------------------------------------------------------------------
type testBinaryTreeNode struct {
	uid   uuid.UUID
	value string
	left  tree.BinaryNode[string]
	right tree.BinaryNode[string]
}

func (n testBinaryTreeNode) String() (str string) {

	if value, present := n.NodeValue(); present {
		str = fmt.Sprintf("%v", value)
	}
	return
}

func (n testBinaryTreeNode) NodeValue() (value string, ok bool) {
	return n.value, n.value != ""
}
func (n testBinaryTreeNode) Left() tree.BinaryNode[string] {
	return n.left
}
func (n testBinaryTreeNode) Right() tree.BinaryNode[string] {
	return n.right
}
func (n testBinaryTreeNode) Equals(rhs tree.BinaryNode[string]) bool {
	nt, ok := rhs.(testBinaryTreeNode)
	return ok && n.uid == nt.uid
}
func createBinaryNode(value string, left tree.BinaryNode[string], right tree.BinaryNode[string]) tree.BinaryNode[string] {
	return testBinaryTreeNode{uuid.New(), value, left, right}
}
func createBinaryEmptyNode(left tree.BinaryNode[string], right tree.BinaryNode[string]) tree.BinaryNode[string] {
	return testBinaryTreeNode{uuid.New(), "", left, right}
}
func createBinaryLeafNode(value string) tree.BinaryNode[string] {
	return createBinaryNode(value, nil, nil)
}
