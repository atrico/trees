package unit_tests

import (
	"testing"

	"gitlab.com/atrico/trees/v2"
	"gitlab.com/atrico/trees/v2/node_matcher"
)

func TestDisplayShowPathSingleNode(t *testing.T) {
	root := createNode("Root")
	showPath := nodeMatcher("Root")
	expected := []string{
		"━━ Root",
	}
	testDisplayShowPathTypes(t, root, showPath, expected, expected, expected, expected)
}

func TestDisplayShowPathSingleChild(t *testing.T) {
	child1 := createNode("Child1")
	root := createNode("Root", child1)
	showPath := nodeMatcher("Child1")
	expectedTopDown := []string{
		"━┱ Root",
		" ┗━━ Child1",
	}
	expectedBalanced := expectedTopDown
	expectedBalancedFavourTop := []string{
		" ┏━━ Child1",
		"━┹ Root",
	}
	expectedBottomUp := expectedBalancedFavourTop
	testDisplayShowPathTypes(t, root, showPath, expectedTopDown, expectedBalanced, expectedBalancedFavourTop, expectedBottomUp)
}

func TestDisplayShowPathSingleLevel(t *testing.T) {
	child1 := createNode("Child1")
	child2 := createNode("Child2")
	child3 := createNode("Child3")
	child4 := createNode("Child4")
	root := createNode("Root", child1, child2, child3, child4)
	showPath := nodeMatcher("Child2")
	expectedTopDown := []string{
		"━┱ Root",
		" ┠── Child1",
		" ┡━━ Child2",
		" ├── Child3",
		" └── Child4",
	}
	expectedBalanced := []string{
		" ┌── Child1",
		" ┢━━ Child2",
		"━╃ Root",
		" ├── Child3",
		" └── Child4",
	}
	expectedBalancedFavourTop := []string{
		" ┌── Child1",
		" ┢━━ Child2",
		"━╃ Root",
		" ├── Child3",
		" └── Child4",
	}
	expectedBottomUp := []string{
		" ┌── Child1",
		" ┢━━ Child2",
		" ┠── Child3",
		" ┠── Child4",
		"━┹ Root",
	}
	testDisplayShowPathTypes(t, root, showPath, expectedTopDown, expectedBalanced, expectedBalancedFavourTop, expectedBottomUp)
}

func TestDisplayShowPathManyLevelChildren(t *testing.T) {
	// Arrange
	child111 := createNode("Child111")
	child11 := createNode("Child11", child111)
	child121 := createNode("Child121")
	child12 := createNode("Child12", child121)
	child1 := createNode("Child1", child11, child12)
	child211 := createNode("Child211")
	child21 := createNode("Child21", child211)
	child221 := createNode("Child221")
	child22 := createNode("Child22", child221)
	child2 := createNode("Child2", child21, child22)
	root := createNode("Root", child1, child2)
	showPath := nodeMatcher("Child121")
	expectedTopDown := []string{
		"━┱ Root",
		" ┡━┱ Child1",
		" │ ┠─┬ Child11",
		" │ ┃ └── Child111",
		" │ ┗━┱ Child12",
		" │   ┗━━ Child121",
		" └─┬ Child2",
		"   ├─┬ Child21",
		"   │ └── Child211",
		"   └─┬ Child22",
		"     └── Child221",
	}
	expectedBalanced := []string{
		"   ┌─┬ Child11",
		"   │ └── Child111",
		" ┏━╅ Child1",
		" ┃ ┗━┱ Child12",
		" ┃   ┗━━ Child121",
		"━╃ Root",
		" │ ┌─┬ Child21",
		" │ │ └── Child211",
		" └─┼ Child2",
		"   └─┬ Child22",
		"     └── Child221",
	}
	expectedBalancedFavourTop := []string{
		"     ┌── Child111",
		"   ┌─┴ Child11",
		" ┏━╅ Child1",
		" ┃ ┃ ┏━━ Child121",
		" ┃ ┗━┹ Child12",
		"━╃ Root",
		" │   ┌── Child211",
		" │ ┌─┴ Child21",
		" └─┼ Child2",
		"   │ ┌── Child221",
		"   └─┴ Child22",
	}
	expectedBottomUp := []string{
		"     ┌── Child111",
		"   ┌─┴ Child11",
		"   │ ┏━━ Child121",
		"   ┢━┹ Child12",
		" ┏━┹ Child1",
		" ┃   ┌── Child211",
		" ┃ ┌─┴ Child21",
		" ┃ │ ┌── Child221",
		" ┃ ├─┴ Child22",
		" ┠─┴ Child2",
		"━┹ Root",
	}
	testDisplayShowPathTypes(t, root, showPath, expectedTopDown, expectedBalanced, expectedBalancedFavourTop, expectedBottomUp)
}

func TestDisplayShowPathEmptyNodes(t *testing.T) {
	// Arrange
	child111 := createNode("Child111")
	child11 := createNode("Child11", child111)
	child121 := createNode("Child121")
	child12 := createEmptyNode(child121)
	child1 := createNode("Child1", child11, child12)
	child211 := createNode("Child211")
	child21 := createNode("Child21", child211)
	child221 := createNode("Child221")
	child22 := createNode("Child22", child221)
	child2 := createNode("Child2", child21, child22)
	root := createEmptyNode(child1, child2)
	showPath := nodeMatcher("Child21")
	expectedTopDown := []string{
		"━┓",
		" ┠─┬ Child1",
		" ┃ ├─┬ Child11",
		" ┃ │ └── Child111",
		" ┃ └─┐",
		" ┃   └── Child121",
		" ┗━┱ Child2",
		"   ┡━┯ Child21",
		"   │ └── Child211",
		"   └─┬ Child22",
		"     └── Child221",
	}
	expectedBalanced := []string{
		"   ┌─┬ Child11",
		"   │ └── Child111",
		" ┌─┼ Child1",
		" │ └─┐",
		" │   └── Child121",
		"━┪",
		" ┃ ┏━┯ Child21",
		" ┃ ┃ └── Child211",
		" ┗━╃ Child2",
		"   └─┬ Child22",
		"     └── Child221",
	}
	expectedBalancedFavourTop := []string{
		"     ┌── Child111",
		"   ┌─┴ Child11",
		" ┌─┼ Child1",
		" │ │ ┌── Child121",
		" │ └─┘",
		"━┪",
		" ┃   ┌── Child211",
		" ┃ ┏━┷ Child21",
		" ┗━╃ Child2",
		"   │ ┌── Child221",
		"   └─┴ Child22",
	}
	expectedBottomUp := []string{
		"     ┌── Child111",
		"   ┌─┴ Child11",
		"   │ ┌── Child121",
		"   ├─┘",
		" ┌─┴ Child1",
		" │   ┌── Child211",
		" │ ┏━┷ Child21",
		" │ ┃ ┌── Child221",
		" │ ┠─┴ Child22",
		" ┢━┹ Child2",
		"━┛",
	}
	testDisplayShowPathTypes(t, root, showPath, expectedTopDown, expectedBalanced, expectedBalancedFavourTop, expectedBottomUp)
}

func TestDisplayShowPathEmptyNodesAllEmpty(t *testing.T) {
	// Arrange
	child11 := createNode("Child11")
	child12 := createNode("Child12")
	child1 := createEmptyNode(child11, child12)
	child21 := createNode("Child21")
	child22 := createNode("Child22")
	child2 := createEmptyNode(child21, child22)
	root := createEmptyNode(child1, child2)
	showPath := nodeMatcher("Child21")
	expectedTopDown := []string{
		"━┓",
		" ┠─┐",
		" ┃ ├── Child11",
		" ┃ └── Child12",
		" ┗━┓",
		"   ┡━━ Child21",
		"   └── Child22",
	}
	expectedBalanced := []string{
		"   ┌── Child11",
		" ┌─┤",
		" │ └── Child12",
		"━┪",
		" ┃ ┏━━ Child21",
		" ┗━┩",
		"   └── Child22",
	}
	expectedBalancedFavourTop := expectedBalanced
	expectedBottomUp := []string{
		"   ┌── Child11",
		"   ├── Child12",
		" ┌─┘",
		" │ ┏━━ Child21",
		" │ ┠── Child22",
		" ┢━┛",
		"━┛",
	}
	testDisplayShowPathTypes(t, root, showPath, expectedTopDown, expectedBalanced, expectedBalancedFavourTop, expectedBottomUp)
}

func TestDisplayShowPathMultipleNodes(t *testing.T) {
	// Arrange
	child111 := createNode("Child111")
	child11 := createNode("Child11", child111)
	child121 := createNode("Child121")
	child12 := createNode("Child12", child121)
	child1 := createEmptyNode(child11, child12)
	child211 := createNode("Child211")
	child21 := createNode("Child21", child211)
	child221 := createNode("Child221")
	child22 := createNode("Child22", child221)
	child2 := createNode("Child2", child21, child22)
	root := createNode("Root", child1, child2)
	showPath := tree_node_matcher.Or(
		nodeMatcher("Root"),
		nodeMatcher("Child2"),
		nodeMatcher("Child121"))
	expectedTopDown := []string{
		"━┳ Root",
		" ┣━┓",
		" ┃ ┠─┬ Child11",
		" ┃ ┃ └── Child111",
		" ┃ ┗━┱ Child12",
		" ┃   ┗━━ Child121",
		" ┗━┯ Child2",
		"   ├─┬ Child21",
		"   │ └── Child211",
		"   └─┬ Child22",
		"     └── Child221",
	}
	expectedBalanced := []string{
		"   ┌─┬ Child11",
		"   │ └── Child111",
		" ┏━┪",
		" ┃ ┗━┱ Child12",
		" ┃   ┗━━ Child121",
		"━╋ Root",
		" ┃ ┌─┬ Child21",
		" ┃ │ └── Child211",
		" ┗━┿ Child2",
		"   └─┬ Child22",
		"     └── Child221",
	}
	expectedBalancedFavourTop := []string{
		"     ┌── Child111",
		"   ┌─┴ Child11",
		" ┏━┪",
		" ┃ ┃ ┏━━ Child121",
		" ┃ ┗━┹ Child12",
		"━╋ Root",
		" ┃   ┌── Child211",
		" ┃ ┌─┴ Child21",
		" ┗━┿ Child2",
		"   │ ┌── Child221",
		"   └─┴ Child22",
	}
	expectedBottomUp := []string{
		"     ┌── Child111",
		"   ┌─┴ Child11",
		"   │ ┏━━ Child121",
		"   ┢━┹ Child12",
		" ┏━┛",
		" ┃   ┌── Child211",
		" ┃ ┌─┴ Child21",
		" ┃ │ ┌── Child221",
		" ┃ ├─┴ Child22",
		" ┣━┷ Child2",
		"━┻ Root",
	}
	testDisplayShowPathTypes(t, root, showPath, expectedTopDown, expectedBalanced, expectedBalancedFavourTop, expectedBottomUp)
}

func testDisplayShowPathTypes(t *testing.T, root tree.Node[string], showPath tree.NodeMatcher[string], expectedTopDown []string, expectedBalanced []string, expectedBalancedFavourTop []string, expectedBottomUp []string) {
	testConfig := newTestConfig().ForStandardTree(root).WithShowPath(showPath)
	testDisplayTypesImpl(t, testConfig, expectedTopDown, expectedBalanced, expectedBalancedFavourTop, expectedBottomUp)
}
