package unit_tests

import (
	"strconv"
	"testing"

	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
	"gitlab.com/atrico/trees/v2"
	tree_node_matcher "gitlab.com/atrico/trees/v2/node_matcher"
)

func Test_NodeMatcher_FromFunc(t *testing.T) {
	// Arrange
	f := matchFunc(eq(2))
	matcher := tree.MakeNodeMatcher(f)

	// Act
	result1 := matcher.IsMatch(getNode(1))
	result2 := matcher.IsMatch(getNode(2))
	result3 := matcher.IsMatch(getNode(3))

	// Assert
	assert.That(t, result1, is.False, "1")
	assert.That(t, result2, is.True, "2")
	assert.That(t, result3, is.False, "3")
}

func Test_NodeMatcher_Not(t *testing.T) {
	// Arrange
	f := matchFunc(eq(2))
	matcher := tree_node_matcher.Not(tree.MakeNodeMatcher(f))

	// Act
	result1 := matcher.IsMatch(getNode(1))
	result2 := matcher.IsMatch(getNode(2))
	result3 := matcher.IsMatch(getNode(3))

	// Assert
	assert.That(t, result1, is.True, "1")
	assert.That(t, result2, is.False, "2")
	assert.That(t, result3, is.True, "3")
}

func Test_NodeMatcher_And(t *testing.T) {
	// Arrange
	m1 := tree.MakeNodeMatcher(matchFunc(odd()))
	m2 := tree.MakeNodeMatcher(matchFunc(gt(1)))
	matcher := tree_node_matcher.And(m1, m2)

	// Act
	result1 := matcher.IsMatch(getNode(1))
	result2 := matcher.IsMatch(getNode(2))
	result3 := matcher.IsMatch(getNode(3))

	// Assert
	assert.That(t, result1, is.False, "1")
	assert.That(t, result2, is.False, "2")
	assert.That(t, result3, is.True, "3")
}

func Test_NodeMatcher_Or(t *testing.T) {
	// Arrange
	m1 := tree.MakeNodeMatcher(matchFunc(lt(2)))
	m2 := tree.MakeNodeMatcher(matchFunc(gt(2)))
	matcher := tree_node_matcher.Or(m1, m2)

	// Act
	result1 := matcher.IsMatch(getNode(1))
	result2 := matcher.IsMatch(getNode(2))
	result3 := matcher.IsMatch(getNode(3))

	// Assert
	assert.That(t, result1, is.True, "1")
	assert.That(t, result2, is.False, "2")
	assert.That(t, result3, is.True, "3")
}

func getNode(i int) tree.Node[string] {
	return createNode(strconv.Itoa(i))
}

func eq(rhs int) func(lhs int) bool {
	return func(lhs int) bool {
		return lhs == rhs
	}
}
func lt(rhs int) func(lhs int) bool {
	return func(lhs int) bool {
		return lhs < rhs
	}
}
func gt(rhs int) func(lhs int) bool {
	return func(lhs int) bool {
		return lhs > rhs
	}
}
func odd() func(lhs int) bool {
	return func(lhs int) bool {
		return (lhs % 2) == 1
	}
}

func matchFunc(match func(rhs int) bool) func(node tree.Node[string]) bool {
	return func(node tree.Node[string]) bool {
		if val, err := strconv.Atoi(NodeValueAsString(node)); err == nil {
			return match(val)
		} else {
			panic("invalid value")
		}
	}
}
