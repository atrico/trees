package unit_tests

import (
	"testing"

	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"

	"gitlab.com/atrico/trees/v2"
)

func Test_FindFirst_NotFoundSingleNode(t *testing.T) {
	// Arrange
	root := createNode("root")

	// Act
	_, here := tree.FindFirst(root, nodeMatcher("not_there"))

	// Assert
	assert.That(t, here, is.False, "Not found")
}

func Test_FindFirst_NotFoundManyNodes(t *testing.T) {
	// Arrange
	child11 := createNode("Child11")
	child12 := createNode("Child12")
	child1 := createNode("Child1", child11, child12)
	child21 := createNode("Child21")
	child2 := createNode("Child2", child21)
	root := createNode("root", child1, child2)

	// Act
	_, here := tree.FindFirst(root, nodeMatcher("not_there"))

	// Assert
	assert.That(t, here, is.False, "Not found")
}

func Test_FindFirst_FoundAtRoot(t *testing.T) {
	// Arrange
	child11 := createNode("Child11")
	child12 := createNode("Child12")
	child1 := createNode("Child1", child11, child12)
	child21 := createNode("Child21")
	child2 := createNode("Child2", child21)
	root := createNode("root", child1, child2)

	// Act
	nodes, here := tree.FindFirst(root, nodeMatcher("root"))

	// Assert
	assert.That(t, here, is.True, "Found")
	assert.That(t, nodes, is.DeepEqualTo(tree.PathToNode[string]([]tree.Node[string]{root})), "Root")
}

func Test_FindFirst_FoundAtChild(t *testing.T) {
	// Arrange
	child11 := createNode("Child11")
	child12 := createNode("Child12")
	child1 := createNode("Child1", child11, child12)
	child21 := createNode("Child21")
	child2 := createNode("Child2", child21)
	root := createNode("root", child1, child2)

	// Act
	nodes, here := tree.FindFirst(root, nodeMatcher("Child1"))

	// Assert
	assert.That(t, here, is.True, "Found")
	assert.That(t, nodes, is.DeepEqualTo(tree.PathToNode[string]([]tree.Node[string]{root, child1})), "Root-Child1")
}

func Test_FindFirst_FoundAtSubChild(t *testing.T) {
	// Arrange
	child11 := createNode("Child11")
	child12 := createNode("Child12")
	child1 := createNode("Child1", child11, child12)
	child21 := createNode("Child21")
	child2 := createNode("Child2", child21)
	root := createNode("root", child1, child2)

	// Act
	nodes, here := tree.FindFirst(root, nodeMatcher("Child12"))

	// Assert
	assert.That(t, here, is.True, "Found")
	assert.That(t, nodes, is.DeepEqualTo(tree.PathToNode[string]([]tree.Node[string]{root, child1, child12})), "Root-Child1-Child12")
}
