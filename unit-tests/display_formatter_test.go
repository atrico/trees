package unit_tests

import (
	"fmt"
	"testing"

	"gitlab.com/atrico/console/ansi"
	"gitlab.com/atrico/console/ansi/color"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
	"gitlab.com/atrico/trees/v2"
	"gitlab.com/atrico/trees/v2/display"
)

func Test_DisplayFormatter(t *testing.T) {
	// Arrange
	root := createNode("0",
		createNode("1",
			createNode("1.1",
				createNode("1.1.1")),
			createNode("1.2")),
		createNode("2",
			createNode("2.1"),
			createNode("2.2",
				createNode("2.2.1"))))
	hasSingleChild := func(node tree.Node[string]) bool {
		return len(node.Children()) == 1
	}
	startsWith1 := func(node tree.Node[string]) bool {
		return NodeValueAsString(node)[0] == '1'
	}
	colorRed := ansi.NewAttributesFore(color.Red)
	prependAsterisk := func(value string) string {
		return fmt.Sprintf("** %v", value)
	}
	formatter := treedisplay.NewNodeFormatterBuilder[string]().
		AddAttributes(tree.MakeNodeMatcher(hasSingleChild), colorRed).
		Add(tree.MakeNodeMatcher(startsWith1), prependAsterisk).
		Build()

	// Act
	lines := treedisplay.DisplayTree(root, treedisplay.NewDisplayConfig[string]().WithFormatter(formatter))
	for _, line := range lines {
		fmt.Println(line)
	}

	// Assert
	assert.That(t, lines, is.EqualTo([]string{
		"   ┌─┬ " + colorRed.ApplyWithResetTo("1.1"),
		"   │ └── ** 1.1.1",
		" ┌─┼ ** 1",
		" │ └── ** 1.2",
		"─┼ 0",
		" │ ┌── 2.1",
		" └─┼ 2",
		"   └─┬ " + colorRed.ApplyWithResetTo("2.2"),
		"     └── 2.2.1",
	}), "correct formatting")
}
