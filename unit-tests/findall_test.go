package unit_tests

import (
	"testing"

	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
	"gitlab.com/atrico/trees/v2/node_matcher"

	"gitlab.com/atrico/trees/v2"
)

func Test_FindAll_NotFoundSingleNode(t *testing.T) {
	// Arrange
	root := createNode("root")

	// Act
	paths := tree.FindAll(root, nodeMatcher("not_there"))

	// Assert
	assert.That(t, paths, is.EquivalentTo([]tree.PathToNode[string]{}), "Not found")
}

func Test_FindAll_NotFoundManyNodes(t *testing.T) {
	// Arrange
	child11 := createNode("Child11")
	child12 := createNode("Child12")
	child1 := createNode("Child1", child11, child12)
	child21 := createNode("Child21")
	child2 := createNode("Child2", child21)
	root := createNode("root", child1, child2)

	// Act
	paths := tree.FindAll(root, nodeMatcher("not_there"))

	// Assert
	assert.That(t, paths, is.EquivalentTo([]tree.PathToNode[string]{}), "Not found")
}

func Test_FindAll_FoundAtRoot(t *testing.T) {
	// Arrange
	child11 := createNode("Child11")
	child12 := createNode("Child12")
	child1 := createNode("Child1", child11, child12)
	child21 := createNode("Child21")
	child2 := createNode("Child2", child21)
	root := createNode("root", child1, child2)

	// Act
	paths := tree.FindAll(root, nodeMatcher("root"))

	// Assert
	assert.That(t, paths, is.EquivalentTo([]tree.PathToNode[string]{{root}}), "Found at root")
}

func Test_FindAll_FoundAtChild(t *testing.T) {
	// Arrange
	child11 := createNode("Child11")
	child12 := createNode("Child12")
	child1 := createNode("Child1", child11, child12)
	child21 := createNode("Child21")
	child2 := createNode("Child2", child21)
	root := createNode("root", child1, child2)

	// Act
	paths := tree.FindAll(root, nodeMatcher("Child1"))

	// Assert
	assert.That(t, paths, is.EquivalentTo([]tree.PathToNode[string]{{root, child1}}), "Found at root-child1")
}

func Test_FindAll_FoundAtSubChild(t *testing.T) {
	// Arrange
	child11 := createNode("Child11")
	child12 := createNode("Child12")
	child1 := createNode("Child1", child11, child12)
	child21 := createNode("Child21")
	child2 := createNode("Child2", child21)
	root := createNode("root", child1, child2)

	// Act
	paths := tree.FindAll(root, nodeMatcher("Child12"))

	// Assert
	assert.That(t, paths, is.EquivalentTo([]tree.PathToNode[string]{{root, child1, child12}}), "Found at root-child1-child12")
}

func Test_FindAll_FoundMultiple(t *testing.T) {
	// Arrange
	child11 := createNode("Child11")
	child12 := createNode("Child12")
	child1 := createNode("Child1", child11, child12)
	child21 := createNode("Child21")
	child2 := createNode("Child2", child21)
	root := createNode("root", child1, child2)

	// Act
	matcher := tree_node_matcher.Or(nodeMatcher("root"), nodeMatcher("Child1"), nodeMatcher("Child12"))
	paths := tree.FindAll(root, matcher)

	// Assert
	assert.That(t, paths, is.EquivalentTo([]tree.PathToNode[string]{{root}, {root, child1}, {root, child1, child12}}), "Found at root-child1-child12")
}
