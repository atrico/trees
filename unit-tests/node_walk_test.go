package unit_tests

import (
	"fmt"
	"testing"

	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
	"gitlab.com/atrico/trees/v2"
	"gitlab.com/atrico/trees/v2/display"
	"gitlab.com/atrico/trees/v2/walk"
)

func Test_Walk(t *testing.T) {
	treeWalkTestImpl := func(t *testing.T, root tree.Node[string], walkFunc func(node tree.Node[string], action func(node tree.Node[string])), expected []string) {
		// Arrange
		visited := make([]string, 0)

		// Act
		if root != nil {
			for _, line := range treedisplay.DisplayTree(root, treedisplay.NewDisplayConfig[string]()) {
				fmt.Println(line)
			}
		}
		walkFunc(root, func(node tree.Node[string]) { visited = append(visited, fmt.Sprintf("%v", NodeValueAsString(node))) })

		// Assert
		assert.That(t, visited, is.EqualTo(expected), "expected nodes")
	}
	for _, testCase := range treeWalkTestCases {
		t.Run(fmt.Sprintf("DepthFirst: %s", testCase.name), func(t *testing.T) {
			treeWalkTestImpl(t, testCase.root, treewalk.DepthFirst[string], testCase.depthFirst)
		})
		t.Run(fmt.Sprintf("BreadthFirst: %s", testCase.name), func(t *testing.T) {
			treeWalkTestImpl(t, testCase.root, treewalk.BreadthFirst[string], testCase.breadthFirst)
		})
	}
}

type treeWalkTestCase struct {
	name         string
	root         tree.Node[string]
	depthFirst   []string
	breadthFirst []string
}

var treeWalkTestCases = []treeWalkTestCase{
	{"Nil",
		nil,
		make([]string, 0),
		make([]string, 0),
	},
	{"Root only",
		createNode("root"),
		[]string{"root"},
		[]string{"root"},
	},
	{"Full tree",
		createNode("0",
			createNode("1",
				createNode("1.1",
					createNode("1.1.1")),
				createNode("1.2")),
			createNode("2",
				createNode("2.1"),
				createNode("2.2",
					createNode("2.2.1")))),
		[]string{"1.1.1", "1.1", "1.2", "1", "2.1", "2.2.1", "2.2", "2", "0"},
		[]string{"0", "1", "2", "1.1", "1.2", "2.1", "2.2", "1.1.1", "2.2.1"},
	},
}
