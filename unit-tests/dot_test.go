package unit_tests

import (
	"fmt"
	"strings"
	"testing"

	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
	"gitlab.com/atrico/trees/v2"
	"gitlab.com/atrico/trees/v2/dot"
)

func Test_Dot(t *testing.T) {
	for _, testCase := range dotTestCases {
		t.Run(testCase.name, func(t *testing.T) {
			// Arrange
			root := createTree()

			// Act
			dot := treedot.CreateDot(root, testCase.config)

			// Assert
			assert.That(t, dot, is.EqualTo(testCase.expected), "dot format")
		})
	}
}

func createTree() tree.Node[string] {
	child111 := createNode("C111")
	child11 := createNode("C11", child111)
	child121 := createNode("C121")
	child12 := createNode("C12", child121)
	child1 := createEmptyNode(child11, child12)
	child211 := createNode("C211")
	child21 := createNode("C21", child211)
	child221 := createNode("C221")
	child22 := createNode("C22", child221)
	child2 := createNode("C2", child21, child22)
	return createNode("Root", child1, child2)
}

type dotTestCase struct {
	name     string
	config   treedot.DotConfig[string]
	expected string
}

var dotTestCases = []dotTestCase{
	{"Simple", treedot.NewDotConfig[string](), `digraph{0[label="Root"];1[label="",width=0.04,height=0.04,style=invis];2[label="C2"];3[label="C11"];4[label="C12"];5[label="C21"];6[label="C22"];7[label="C111"];8[label="C121"];9[label="C211"];10[label="C221"];{rank=min 0;}{rank=same 1,2;}{rank=same 3,4,5,6;}{rank=same 7,8,9,10;}0->1->3->7;0->2->5->9;1->4->8;2->6->10;}`},
	{"Formatters", treedot.NewDotConfig[string]().WithFormatter(formatter1), `digraph{0[label="Root"];1[label="",width=0.04,height=0.04,style=invis];2[label="C2",color=red,shape=box];3[label="C11"];4[label="C12"];5[label="C21",color=red,shape=box];6[label="C22",color=red,shape=box];7[label="leaf:C111"];8[label="leaf:C121"];9[label="leaf:C211",color=red,shape=box];10[label="leaf:C221",color=red,shape=box];{rank=min 0;}{rank=same 1,2;}{rank=same 3,4,5,6;}{rank=same 7,8,9,10;}0->1->3->7;0->2->5->9;1->4->8;2->6->10;}`},
	{"ShowPath", treedot.NewDotConfig[string]().WithShowPath(elevens), `digraph{0[label="Root"];1[label="",width=0.04,height=0.04,style=invis];2[label="C2"];3[label="C11"];4[label="C12"];5[label="C21"];6[label="C22"];7[label="C111"];8[label="C121"];9[label="C211"];10[label="C221"];{rank=min 0;}{rank=same 1,2;}{rank=same 3,4,5,6;}{rank=same 7,8,9,10;}0->2->5->9[color=red];0->1->3[color=red];1->4->8;2->6->10;3->7;}`},
}

var formatter1 = treedot.NewNodeFormatterBuilder[string]().
	Add(noChildren(), labelWith("leaf")).
	AddAttributes(startsWith("C2"), map[string]string{"shape": "box", "color": "red"}).
	Build()

func startsWith(str string) tree.NodeMatcher[string] {
	return tree.MakeNodeMatcher[string](func(node tree.Node[string]) bool {
		value, present := node.GetNodeValue()
		return present && strings.HasPrefix(fmt.Sprintf("%v", value), str)
	})
}

func noChildren() tree.NodeMatcher[string] {
	return tree.MakeNodeMatcher(func(node tree.Node[string]) bool {
		return len(node.Children()) == 0
	})
}

func labelWith(str string) treedot.NodeFormatter[string] {
	return func(node tree.Node[string]) map[string]string {
		var valueStr string
		if value, present := node.GetNodeValue(); present {
			valueStr = fmt.Sprintf("%v", value)
		}
		return map[string]string{treedot.LABEL: fmt.Sprintf("%s:%v", str, valueStr)}
	}
}

var elevens = tree.MakeNodeMatcher(func(node tree.Node[string]) bool {
	var str string
	if value, present := node.GetNodeValue(); present {
		str = fmt.Sprintf("%v", value)
		return str == "C11" || str == "C211"
	}
	return false
})
