package tree

// Binary tree node
type BinaryNode[T any] interface {
	// The value associated with this node
	NodeValue() (value T, ok bool)
	// This node's children
	Left() BinaryNode[T]
	Right() BinaryNode[T]
	// Is this node the same node (or copy of)
	Equals(rhs BinaryNode[T]) bool
}

// ---------------------------------------------------------------------
// Binary tree wrapper
// ---------------------------------------------------------------------
type BinaryNodeWrapper[T any] struct {
	BinaryNode[T]
}

func (n BinaryNodeWrapper[T]) GetNodeValue() (value T, ok bool) {
	if n.BinaryNode != nil {
		value, ok = n.BinaryNode.NodeValue()
	}
	return
}

func (n BinaryNodeWrapper[T]) MustGetNodeValue() (value T) {
	var ok bool
	if value, ok = n.GetNodeValue(); !ok {
		panic(NodeHasNoValueError)
	}
	return
}

func (n BinaryNodeWrapper[T]) Children() []Node[T] {
	if n.BinaryNode != nil && (n.BinaryNode.Left() != nil || n.BinaryNode.Right() != nil) {
		return []Node[T]{BinaryNodeWrapper[T]{n.BinaryNode.Left()}, BinaryNodeWrapper[T]{n.BinaryNode.Right()}}
	}
	return make([]Node[T], 0)
}

func (n BinaryNodeWrapper[T]) Equals(rhs Node[T]) bool {
	switch nt := rhs.(type) {
	case BinaryNodeWrapper[T]:
		if n.BinaryNode != nil {
			return n.BinaryNode.Equals(nt.BinaryNode)
		}
	}
	return false
}
