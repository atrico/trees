package treedot

import (
	"fmt"
	"sort"
	"strconv"
	"strings"

	"gitlab.com/atrico/trees/v2"
	"gitlab.com/atrico/trees/v2/walk"
)

const LABEL = "label"

func CreateDot[T any](root tree.Node[T], config DotConfig[T]) (dotText string) {
	nodeMap := make([]nodeDetails[T], 0)
	ranks := make([][]string, 0)
	treewalk.BreadthFirst(root, func(node tree.Node[T]) {
		nodeId := getNodeId(&nodeMap, node)
		rank := len(tree.MustFindFirst(root, tree.MakeNodeMatcher(func(n tree.Node[T]) bool { return node.Equals(n) }))) - 1
		for rank >= len(ranks) {
			ranks = append(ranks, make([]string, 0))
		}
		ranks[rank] = append(ranks[rank], strconv.Itoa(nodeId))
		for _, child := range node.Children() {
			childId := getNodeId(&nodeMap, child)
			nodeMap[nodeId].links = append(nodeMap[nodeId].links, childId)
		}
		nodeId++
	})
	dotFormat := strings.Builder{}
	dotFormat.WriteString("digraph{")
	for id, node := range nodeMap {
		var attributes map[string]string
		if config.Formatter != nil {
			attributes = config.Formatter(node)
		} else {
			attributes = make(map[string]string)
		}
		dotFormat.WriteString(fmt.Sprintf("%d%s;", id, formatAttributes(node, attributes)))
	}
	// Ranks
	rk := "min"
	for _, rks := range ranks {
		dotFormat.WriteString(fmt.Sprintf("{rank=%s %s;}", rk, strings.Join(rks, ",")))
		rk = "same"
	}
	// Add show path routes
	if config.ShowPath != nil {
		showPaths := tree.FindAll(root, config.ShowPath)
		sort.Slice(showPaths, func(i, j int) bool { return len(showPaths[i]) > len(showPaths[j]) }) // ascending order of length
		for _, path := range showPaths {
			if route := removeRoute(&nodeMap, path); len(route) > 0 {
				dotFormat.WriteString(fmt.Sprintf("%s[color=red];", strings.Join(route, "->")))
			}
		}
	}
	// Optimise links (link ids already sorted ascending)
	route := getNextRoute(&nodeMap)
	for len(route) > 0 {
		dotFormat.WriteString(fmt.Sprintf("%s;", strings.Join(route, "->")))
		route = getNextRoute(&nodeMap)
	}
	dotFormat.WriteString("}")
	dotText = dotFormat.String()
	return dotText
}

type nodeDetails[T any] struct {
	tree.Node[T]
	links []int
}

func getNodeId[T any](nodeMap *[]nodeDetails[T], node tree.Node[T]) (id int) {
	for i, n := range *nodeMap {
		if node.Equals(n.Node) {
			return i
		}
	}
	*nodeMap = append(*nodeMap, nodeDetails[T]{Node: node})
	return len(*nodeMap) - 1
}

func formatAttributes[T any](node tree.Node[T], attributes map[string]string) string {
	// Sort keys
	list := make([]string, len(attributes))
	i := 0
	for k := range attributes {
		list[i] = k
		i++
	}
	sort.Strings(list)
	// Format text
	text := strings.Builder{}
	text.WriteString(fmt.Sprintf("[%s=", LABEL))
	var label string
	var hasLabel bool
	if label, hasLabel = attributes[LABEL]; !hasLabel {
		// Add label if not present
		if value, present := node.GetNodeValue(); present {
			label = fmt.Sprintf("%v", value)
		}
	}
	// Ensure label has quotes and internal quotes are escaped
	label = fmt.Sprintf(`"%s"`, strings.ReplaceAll(label, `"`, `\"`))
	text.WriteString(label)
	if label == `""` && len(attributes) == 0 {
		text.WriteString(",width=0.04,height=0.04,style=invis")
	} else {
		for _, k := range list {
			if k != LABEL {
				text.WriteString(fmt.Sprintf(",%s=%s", k, attributes[k]))
			}
		}
	}
	text.WriteString("]")
	return text.String()
}

func getNextRoute[T any](nodeMap *[]nodeDetails[T]) []string {
	for i := range *nodeMap {
		for len((*nodeMap)[i].links) > 0 {
			return getNextRouteImpl(nodeMap, i)
		}
	}
	return nil
}

func getNextRouteImpl[T any](nodeMap *[]nodeDetails[T], node int) (route []string) {
	route = []string{strconv.Itoa(node)}
	for len((*nodeMap)[node].links) > 0 {
		nextNode := (*nodeMap)[node].links[0]
		(*nodeMap)[node].links = (*nodeMap)[node].links[1:]
		route = append(route, strconv.Itoa(nextNode))
		node = nextNode
	}

	return route
}

func removeRoute[T any](nodeMap *[]nodeDetails[T], request tree.PathToNode[T]) (route []string) {
	node := request[0]
	nodeId := getNodeId(nodeMap, node)
	for _, req := range request[1:] {
		reqId := getNodeId(nodeMap, req)
		if removeValue(&(*nodeMap)[nodeId].links, reqId) > 0 {
			if len(route) == 0 {
				route = []string{strconv.Itoa(nodeId)}
			}
			route = append(route, strconv.Itoa(reqId))
		}
		node = req
		nodeId = reqId
	}
	return route
}

func removeValue(list *[]int, value int) (removed int) {
	newList := make([]int, 0, len(*list))
	for _, v := range *list {
		if v == value {
			removed++
		} else {
			newList = append(newList, v)
		}
	}
	if removed > 0 {
		*list = newList
	}
	return removed
}
