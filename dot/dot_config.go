package treedot

import (
	tree "gitlab.com/atrico/trees/v2"
)

type DotConfig[T any] struct {
	Formatter NodeFormatter[T]
	ShowPath  tree.NodeMatcher[T]
}

func NewDotConfig[T any]() DotConfig[T] {
	return DotConfig[T]{}
}

func (c DotConfig[T]) WithFormatter(formatter NodeFormatter[T]) DotConfig[T] {
	return DotConfig[T]{Formatter: formatter, ShowPath: c.ShowPath}
}

func (c DotConfig[T]) WithShowPath(matcher tree.NodeMatcher[T]) DotConfig[T] {
	return DotConfig[T]{Formatter: c.Formatter, ShowPath: matcher}
}
