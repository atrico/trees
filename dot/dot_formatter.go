package treedot

import (
	tree "gitlab.com/atrico/trees/v2"
)

type NodeFormatter[T any] func(node tree.Node[T]) (attributes map[string]string)

type NodeFormatterBuilder[T any] interface {
	Add(matcher tree.NodeMatcher[T], nodeFormatter NodeFormatter[T]) NodeFormatterBuilder[T]
	AddAttributes(matcher tree.NodeMatcher[T], attributes map[string]string) NodeFormatterBuilder[T]
	Build() NodeFormatter[T]
}

func NewNodeFormatterBuilder[T any]() NodeFormatterBuilder[T] {
	return &formatter[T]{}
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------

type formatter[T any] struct {
	matchers   []tree.NodeMatcher[T]
	formatters []NodeFormatter[T]
}

func (f *formatter[T]) Add(matcher tree.NodeMatcher[T], nodeFormatter NodeFormatter[T]) NodeFormatterBuilder[T] {
	f.matchers = append(f.matchers, matcher)
	f.formatters = append(f.formatters, nodeFormatter)
	return f
}

func (f *formatter[T]) AddAttributes(matcher tree.NodeMatcher[T], attributes map[string]string) NodeFormatterBuilder[T] {
	return f.Add(matcher, func(node tree.Node[T]) map[string]string {
		return attributes
	})
}

func (f *formatter[T]) Build() NodeFormatter[T] {
	return func(node tree.Node[T]) (attrs map[string]string) {
		attrs = make(map[string]string)
		for i, matcher := range f.matchers {
			if matcher.IsMatch(node) {
				for k, v := range f.formatters[i](node) {
					attrs[k] = v
				}
			}
		}
		return attrs
	}
}
