package tree_node_matcher

import (
	tree "gitlab.com/atrico/trees/v2"
)

func True[T any]() tree.NodeMatcher[T]  { return nodeMatcherBoolean[T](true) }
func False[T any]() tree.NodeMatcher[T] { return nodeMatcherBoolean[T](false) }

func Not[T any](matcher tree.NodeMatcher[T]) tree.NodeMatcher[T] {
	switch m := matcher.(type) {
	case nil:
		return nil
	case nodeMatcherBoolean[T]:
		return !m
	case nodeMatcherNot[T]:
		return m.matcher
	default:
		return nodeMatcherNot[T]{matcher}
	}
}

func And[T any](matchers ...tree.NodeMatcher[T]) tree.NodeMatcher[T] {
	var matchList []tree.NodeMatcher[T]
	hasMatcher := false
	for _, matcher := range matchers {
		switch m := matcher.(type) {
		case nil:
			// Ignore
		case nodeMatcherBoolean[T]:
			if m {
				// Make sure matcher isn't empty (but don't repeat true)
				hasMatcher = true
			} else {
				// Shortcut, whole match must be false
				return m
			}
		case nodeMatcherAnd[T]:
			// Create single and
			matchList = append(matchList, m.matchers...)
		default:
			matchList = append(matchList, m)
		}
	}
	if len(matchList) > 0 {
		return nodeMatcherAnd[T]{matchList}
	} else if hasMatcher {
		return True[T]()
	} else {
		return nil
	}
}

func Or[T any](matchers ...tree.NodeMatcher[T]) tree.NodeMatcher[T] {
	var matchList []tree.NodeMatcher[T]
	hasMatcher := false
	for _, matcher := range matchers {
		switch m := matcher.(type) {
		case nil:
			// Ignore
		case nodeMatcherBoolean[T]:
			if m {
				// Shortcut, whole match must be true
				return m
			} else {
				// Make sure matcher isn't empty (but don't repeat false)
				hasMatcher = true
			}
		case nodeMatcherOr[T]:
			// Create single or
			matchList = append(matchList, m.matchers...)
		default:
			matchList = append(matchList, m)
		}
	}
	if len(matchList) > 0 {
		return nodeMatcherOr[T]{matchList}
	} else if hasMatcher {
		return False[T]()
	} else {
		return nil
	}
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------
type nodeMatcherBoolean[T any] bool

func (n nodeMatcherBoolean[T]) IsMatch(_ tree.Node[T]) bool {
	return bool(n)
}

type nodeMatcherNot[T any] struct {
	matcher tree.NodeMatcher[T]
}

func (n nodeMatcherNot[T]) IsMatch(node tree.Node[T]) bool {
	return !n.matcher.IsMatch(node)
}

type nodeMatcherAnd[T any] struct {
	matchers []tree.NodeMatcher[T]
}

func (n nodeMatcherAnd[T]) IsMatch(node tree.Node[T]) bool {
	for _, match := range n.matchers {
		if !match.IsMatch(node) {
			return false
		}
	}
	return true
}

type nodeMatcherOr[T any] struct {
	matchers []tree.NodeMatcher[T]
}

func (n nodeMatcherOr[T]) IsMatch(node tree.Node[T]) bool {
	for _, match := range n.matchers {
		if match.IsMatch(node) {
			return true
		}
	}
	return false
}
