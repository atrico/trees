package treewalk

import (
	"gitlab.com/atrico/trees/v2"
)

func DepthFirst[T any](node tree.Node[T], action func(node tree.Node[T])) {
	if node != nil {
		for _, child := range node.Children() {
			DepthFirst(child, action)
		}
		action(node)
	}
}

func BreadthFirst[T any](node tree.Node[T], action func(node tree.Node[T])) {
	if node != nil {
		queue := make([]tree.Node[T], 1)
		queue[0] = node
		for len(queue) > 0 {
			action(queue[0])
			queue = append(queue[1:], queue[0].Children()...)
		}
	}
}
